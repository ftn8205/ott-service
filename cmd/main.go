package main

import (
	_ "ottService/docs"
	"ottService/internal/router"
)

// @title Streaming Service
// @version 1.0
// @description Streaming Service

// @contact.name ian ke

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host localhost:8080
// schemes http
func main() {

	r := router.NewRouter()

	r.Run()

}