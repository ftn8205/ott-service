package hello

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// @BasePath /v1

// Hello godoc
// @Summary ping example
// @Schemes
// @Description do ping
// @Tags example
// @Accept json
// @Produce json
// @Success 200 {string} Hello
// @Router /v1/hello [get]
func Hello(c *gin.Context) {

	c.JSON(http.StatusOK, `{"name":"hello"}`)
}
