### API Doc

1. install swag

```shell
go get github.com/swaggo/swag/cmd/swag
```

2. init swagger

```shell
swag init -d ./cmd
```

3. add comment at main.go, and import this

```go
import _ "templateService/docs"
```

4. add comment at api

5. gen swagger with api doc

```shell
 swag init -d ./cmd,./internal/hello
```

6. add router path

7. run server, and open swagger
```shell
http://localhost:8080/swagger/index.html
```

### Reference
https://github.com/swaggo/swag